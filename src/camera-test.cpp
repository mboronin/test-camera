#include <QtQuick>
#include <sailfishapp.h>
#include "CameraTester.h"

int main(int argc, char *argv[])
{
    qmlRegisterType<CameraTester>("Camera.Test", 1, 0, "CameraTester");
    return SailfishApp::main(argc, argv);
}

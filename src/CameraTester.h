#ifndef CAMERATESTER_H
#define CAMERATESTER_H

#include <QObject>
#include <QVariant>
#include <QVideoEncoderSettings>
#include <QMediaRecorder>
#include <QVideoProbe>

class CameraTester : public QObject
{
    Q_OBJECT
public:
    explicit CameraTester(QObject *parent = nullptr);
    Q_INVOKABLE void testMediaObject(QObject *qmlCamera);

signals:

public slots:
};

#endif // CAMERATESTER_H

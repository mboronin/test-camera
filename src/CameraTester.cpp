#include "CameraTester.h"
#include <QDebug>
#include <QCamera>
#include <QFile>
#include <QUrl>

CameraTester::CameraTester(QObject *parent) : QObject(parent)
{
}


void CameraTester::testMediaObject(QObject *qmlCamera)
{
    QCamera *camera = qvariant_cast<QCamera *>(qmlCamera->property("mediaObject"));
    // todo
    QVideoEncoderSettings videoSettings;
    QMediaRecorder *recorder = new QMediaRecorder(camera);
    QAudioEncoderSettings audioEncoderSettings;
    audioEncoderSettings.setSampleRate(48000);
    audioEncoderSettings.setBitRate(96);
    audioEncoderSettings.setChannelCount(1);
    audioEncoderSettings.setCodec("audio/mpeg, mpegversion=(int)4");
    recorder->setAudioSettings(audioEncoderSettings);
    QVideoEncoderSettings videoEncoderSettings;
    videoEncoderSettings.setResolution(1280, 720);
    videoEncoderSettings.setFrameRate(30);
    videoEncoderSettings.setCodec("video/x-h264");
    recorder->setVideoSettings(videoEncoderSettings);
    recorder->setContainerFormat("video/quicktime, variant=(string)iso");
    recorder->setOutputLocation(QUrl::fromLocalFile("/home/nemo/Videos"));
    /*QString file = "/home/nemo/record.avi";
    QFile ffile(file);
    ffile.open(QIODevice::WriteOnly);
    recorder->setContainerFormat("avi");
    recorder->setOutputLocation(QUrl::fromLocalFile(file));*/
    recorder->record();
    qDebug()<<camera;
    qDebug()<<recorder->state();
    qDebug()<<recorder->status();
    qDebug()<<recorder->error();
}

import QtQuick 2.0
import Sailfish.Silica 1.0
import QtMultimedia 5.5
import QtSensors 5.1
import org.nemomobile.time 1.0

import Camera.Test 1.0


Page {
    id: captureView

    property bool _complete
    property bool _unload
    property bool _captureOnFocus
    property bool captureBusy

    property bool cameraPrimary: true
    readonly property string cameraDevice: cameraPrimary ? "primary" : "secondary"

    property int captureOrientation

    property int _recordingDuration: ((clock.enabled ? clock.time : captureView._endTime) - captureView._startTime) / 1000
    property var _startTime: new Date()
    property var _endTime: _startTime

    function reload() {
        if (captureView._complete) {
            captureView._unload = true
        }
    }

    function _triggerCapture() {
        if (captureTimer.running) {
            captureTimer.reset()
        } else if (camera.videoRecorder.recorderState == CameraRecorder.RecordingState) {
            camera.videoRecorder.stop()
        } else if (camera.captureMode == Camera.CaptureStillImage) {
            camera.captureImage()
        } else {
            camera.record()
        }
    }

    function _resetFocus() {
        camera.unlock()
    }

    function writeMetaData() {
        captureView.captureOrientation = sensor.rotationAngle
        // Camera documentation says dateTimeOriginal should be used but at the moment CameraBinMetaData uses only
        // date property (which the documentation doesn't even list)
        camera.metaData.date = new Date()
    }


    onCameraDeviceChanged: {
        // We must call reload() first so camera reaches UnloadedState
        // If we switch Camera::deviceId then camera will not start again
        // which seems to be a bug in QtMultimedia
        // Qt bug: https://bugreports.qt.io/browse/QTBUG-46995
        reload()
        _resetFocus()
        camera.deviceId = cameraDevice
    }


    Component.onCompleted: {
        _complete = true
    }

    CameraTester { id: cameraTester }

    Timer {
        id: reloadTimer
        interval: 100
        running: captureView._unload && camera.cameraStatus == Camera.UnloadedStatus
        onTriggered: {
            captureView._unload = false
        }
    }

    Timer {
        id: startFailedTimer
        interval: 2000
        onTriggered: {
            if (camera.cameraStatus === Camera.StartingStatus) {
                captureView.reload()
            }
        }
    }


    VideoOutput {
        id: output
        anchors.fill: parent
        source: camera
        property bool mirror: !cameraPrimary
    }

    Camera {
        id: camera

        function captureImage() {
            if (camera.lockStatus != Camera.Searching) {
                _completeCapture()
            } else {
                captureView._captureOnFocus = true
            }
        }

        function _completeCapture() {
            if (captureBusy) {
                _captureQueued = true
                return
            }

            captureBusy = true

            camera.imageCapture.captureToLocation("/home/nemo/Pictures/")
        }

        function _finishRecording() {
            if (videoRecorder.recorderState == CameraRecorder.StoppedState) {
                videoRecorder.recorderStateChanged.disconnect(_finishRecording)
            }
        }


        captureMode: Camera.CaptureStillImage

        onCaptureModeChanged: captureView._resetFocus()

        cameraState: captureView._complete && !captureView._unload
                    ? Camera.ActiveState
                    : Camera.UnloadedState

        onCameraStateChanged: {
            if (cameraState == Camera.ActiveState) {
                console.log("Camera.ActiveState")
            } else if (cameraState == Camera.LoadedState) {
                console.log("Camera.LoadedState")
            } else if (cameraState == Camera.UnloadedState) {
                console.log("Camera.UnloadedState")
            }
        }

        onCameraStatusChanged: {
            if (camera.cameraStatus == Camera.StartingStatus) {
                startFailedTimer.restart()
            } else {
                startFailedTimer.stop()
            }
        }

        imageCapture {
            resolution: "1280x720"
            onResolutionChanged: reload()

            onImageSaved: {
                camera.unlock()
                captureBusy = false
                console.log(path)
            }
            onCaptureFailed: {
                camera.unlock()
                captureBusy = false
                console.log("Capture failed")
            }
        }
        videoRecorder {
            resolution: "1280x720"
            onResolutionChanged: reload()
            frameRate: 30
            audioSampleRate: 48000
            audioBitRate: 96
            audioChannels: 1
            audioCodec: "audio/mpeg, mpegversion=(int)4"
            videoCodec: "video/x-h264"
            mediaContainer: "video/quicktime, variant=(string)iso"
            outputLocation: "/home/nemo/Videos"
            onRecorderStateChanged: {
                if (camera.videoRecorder.recorderState == CameraRecorder.StoppedState) {
                    console.log("saved to: " + camera.videoRecorder.outputLocation)
                }
            }
        }
        focus {
            // could expect that locking focus on auto or continous behaves the same, but
            // continuous doesn't work as well
            focusMode: {
                focusMode: Camera.FocusContinuous
                focusPointMode: Camera.FocusPointAuto
            }
        }
        flash.mode: Camera.FlashAuto


        viewfinder {
            resolution: "1280x720"
            minimumFrameRate: 30
            maximumFrameRate: 30
        }

        metaData {
            orientation: captureOrientation
        }

        onDeviceIdChanged: captureView.reload()
        viewfinder.onResolutionChanged: captureView.reload()
        focus.onFocusModeChanged: camera.unlock()

        onLockStatusChanged: {
            if (lockStatus != Camera.Searching && captureView._captureOnFocus) {
                captureView._captureOnFocus = false
                camera._completeCapture()
            }
        }

    }

    Column {
        anchors.right: parent.right
        anchors.top: parent.top

        Button {
            text: cameraDevice
            onClicked: {
                cameraPrimary = !cameraPrimary
            }
        }

        Button {
            text: camera.captureMode == Camera.CaptureStillImage ? "Photo" : "Video"
            onClicked: {
                if (camera.captureMode == Camera.CaptureStillImage) {
                    camera.captureMode = Camera.CaptureVideo
                } else {
                    camera.captureMode = Camera.CaptureStillImage
                }
            }
        }
    }

    Column {
        anchors.right: parent.right
        anchors.bottom: parent.bottom

        Rectangle {
            radius: Theme.paddingSmall / 2
            anchors.horizontalCenter: parent.horizontalCenter
            width: timerLabel.implicitWidth + (2 * Theme.paddingMedium)
            height: timerLabel.implicitHeight + (2 * Theme.paddingSmall)
            color: Theme.highlightBackgroundColor
            visible: camera.captureMode == Camera.CaptureVideo
            opacity: 0.6

            Label {
                id: timerLabel

                anchors.centerIn: parent

                text: Format.formatDuration(
                          captureView._recordingDuration,
                          captureView._recordingDuration >= 3600 ? Formatter.DurationLong : Formatter.DurationShort)
                font.pixelSize: Theme.fontSizeMedium

            }
        }

        Button {
            text: camera.captureMode == Camera.CaptureVideo
                  ? camera.videoRecorder.recorderState == CameraRecorder.RecordingState ? "Finish!" : "Acion!"
                  : "Smile!"
            onPressed: {
                if (camera.captureMode == Camera.CaptureStillImage) {
                    console.log("shutter pressed")
                    if (camera.captureMode == Camera.CaptureStillImage
                            && camera.lockStatus == Camera.Unlocked) {
                        camera.searchAndLock()
                    }
                }
            }
            onReleased: {
                if (camera.captureMode == Camera.CaptureStillImage) {
                    console.log("shutter released")
                    if (camera.lockStatus == Camera.Locked) {
                        extensions.captureTime = new Date()
                        camera.imageCapture.capture()
                    }
                }
            }
            onClicked: {
                if (camera.videoRecorder.recorderState == CameraRecorder.RecordingState) {
                    camera.videoRecorder.stop()
                } else if (camera.captureMode == Camera.CaptureStillImage) {

                    console.log("shutter clicked")
                    camera.captureImage()
                } else {
                    cameraTester.testMediaObject(camera);
//                    camera.videoRecorder.record()
//                    if (camera.videoRecorder.recorderState == CameraRecorder.RecordingState) {
//                        camera.videoRecorder.recorderStateChanged.connect(camera._finishRecording)
//                    }
                }
            }
        }

        WallClock {
            id: clock
            updateFrequency: WallClock.Second
            enabled: camera.videoRecorder.recorderState == CameraRecorder.RecordingState
            onEnabledChanged: {
                if (enabled) {
                    captureView._startTime = clock.time
                    captureView._endTime = captureView._startTime
                } else {
                    captureView._endTime = captureView._startTime
                }
            }
        }
    }

    OrientationSensor {
        id: sensor
        active: true
        property int rotationAngle: reading.orientation ? _getOrientation(reading.orientation) : 0
        function _getOrientation(value) {
            switch (value) {
            case 1:
                return 0
            case 2:
                return 180
            case 3:
                return 270
            default:
                return 90
            }
        }
    }
}
